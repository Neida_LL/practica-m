import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {


  form!:FormGroup
  constructor(private fb:FormBuilder) { 
    this.cargarForm();
    console.log(this.form.controls);
    
  }

  ngOnInit(): void {
  }

  get utilesEscolares(){
    console.log(this.form.get('name'));
    
    return this.form.get('utiles') as FormArray;
  }
  
   
  cargarForm(){
    this.form = this.fb.group({
      
      utilescolar:this.fb.group({
         n:['hjkmgchgh'],
         chick:[true]
       }),
       nombre:['ghjfgjh',Validators.pattern(/^[a-zA-zñÑ\s]+$/)],
       check:[true],
       utiles: this.fb.array([[
         
       ]])
    })
  }

  //En conjunto:
  deleteAll(){
    this.utilesEscolares.clear()
  }


  adicionMaterial():void{
    this.utilesEscolares.push(this.fb.control(''));
  }

  deleteUtilEscolar(id:number){
    this.utilesEscolares.removeAt(id);
  }


}
