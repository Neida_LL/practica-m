export interface ElementoI{
  name: string;
  valor:number;
  hasChildren:false;
  childrens: ElementoI[]; 
}