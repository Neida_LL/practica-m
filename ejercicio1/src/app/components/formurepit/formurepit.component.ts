import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-formurepit',
  templateUrl: './formurepit.component.html',
  styleUrls: ['./formurepit.component.css']
})
export class FormurepitComponent implements OnInit {

  node=['banana','nene','mmm']
  form!:FormGroup;
  constructor(private fb:FormBuilder) { }

  ngOnInit(): void {
  }

   cargarFormulario(){
     this.form = this.fb.group({
       cantNiveles:[0],
       cantRepeticiones:[0],
       valor1:['',Validators.required],
       valor2:['',Validators.required],
       valor3:['',Validators.required],
       valor4:['',Validators.required],
     })
   }
}
