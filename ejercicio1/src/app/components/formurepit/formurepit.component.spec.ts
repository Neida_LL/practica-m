import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormurepitComponent } from './formurepit.component';

describe('FormurepitComponent', () => {
  let component: FormurepitComponent;
  let fixture: ComponentFixture<FormurepitComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormurepitComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormurepitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
